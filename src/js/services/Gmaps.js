export default class Gmaps {
    constructor(selector, options) {
        this.el = document.querySelector(selector)
        if (!this.el) {
            console.log(`Not found selector ${selector}`)
        }
        this.selector = selector
        this.options = options
        this.map = new google.maps.Map(this.el, options)
        return this
    }
}
