import Helpers from '../utils/Helpers'

export default class Navigation {
    constructor(headerSelector, hamburgerSelector, navSelector, activeClassName = 'active') {
        this.headerParent = document.querySelector('header.header')
        this.headerEl = document.querySelector(`${headerSelector}`)
        this.hamburgerEl = this.headerEl.querySelector(`${hamburgerSelector}`)
        this.navEl = this.headerEl.querySelector(`${navSelector}`)
        this.activeClass = activeClassName
        this.init()
    }

    _showMenu() {
        this.headerParent.classList.add(this.activeClass)
        this.headerEl.classList.add(this.activeClass)
        this.hamburgerEl.classList.add(this.activeClass)
        this._showNav()
        // this._appendBackdrop()
    }

    _hideMenu() {
        this.headerParent.classList.remove(this.activeClass)
        this.headerEl.classList.remove(this.activeClass)
        this.hamburgerEl.classList.remove(this.activeClass)
        this._hideNav()

        // this._removeBackdrop()
    }

    _showNav() {
        this.navEl.style.height = `${this.navEl.scrollHeight}px`
    }

    _hideNav() {
        this.navEl.style.height = 0
    }

    //   _createBackdrop() {
    //     this.backdrop = document.createElement('div')
    //     this.backdrop.classList.add('backdrop')
    //     this.backdrop.addEventListener('click', () => this._hideMenu())
    //   }

    //   _appendBackdrop() {
    //     document.body.appendChild(this.backdrop)
    //     requestAnimationFrame(() =>
    //       setTimeout(() => {
    //         this.backdrop.classList.add('active')
    //       })
    //     )
    //   }

    //   _removeBackdrop() {
    //     this.backdrop.classList.remove('active')
    //     document.body.removeChild(this.backdrop)
    //   }

    _scrollHandler() {
        if (window.scrollY <= 50) this.headerParent.classList.remove('moved')
        else this.headerParent.classList.add('moved')
    }

    _bindInitialEvents() {
        this.hamburgerEl.addEventListener('click', () => {
            this.headerEl.classList.contains(this.activeClass) ? this._hideMenu() : this._showMenu()
        })
        this.headerEl.addEventListener('click', (e) => {
            if (e.target.hash) this._hideMenu()
        })
        document.addEventListener('scroll', Helpers.debounceEvent(this._scrollHandler.bind(this), 50))
    }

    init() {
        // this._createBackdrop()
        this._bindInitialEvents()
    }
}
