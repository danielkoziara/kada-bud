import Siema from 'siema'

Siema.prototype.addPagination = function () {
  const wrapper = document.createElement('div')
  wrapper.classList.add('siema-nav')
  this.dots = []

  for (let i = 0; i < this.innerElements.length; i++) {
    const btn = document.createElement('button')
    btn.classList.add('siema-dot')
    btn.addEventListener('click', () => {
      this.goTo(i)
      this.dots.forEach((dot) => dot.classList.remove('active'))
      btn.classList.add('active')
    })
    this.dots.push(btn)
    wrapper.appendChild(btn)
  }
  this.dots[this.currentSlide].classList.add('active')
  this.selector.insertAdjacentElement('afterend', wrapper)
}

if (document.querySelector('.presentation-slider__container')) {
  const presentation = new Siema({
    selector: '.presentation-slider__container',
    duration: 500,
    startIndex: 0,
    easing: 'ease-out',
    perPage: {
      768: 2,
    },
    loop: true,
    onChange: function () {
      this.dots.forEach((dot) => dot.classList.remove('active'))
      if (this.currentSlide === -1) {
        this.dots[this.dots.length - 1].classList.add('active')
      } else {
        this.dots[this.currentSlide].classList.add('active')
      }
    },
  })

  presentation.addPagination()

  setInterval(() => presentation.next(), 2000)
}
