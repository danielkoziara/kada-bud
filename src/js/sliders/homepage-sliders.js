import Siema from '../utils/Siema'

if (document.querySelector('.home-slider__container')) {
    const homepage = new Siema({
        selector: '.home-slider__container',
        duration: 500,
        easing: 'ease-out',
        perPage: 1,
        startIndex: 0,
        threshold: 20,
        loop: true,
        onInit: () => {},
        onChange: function () {
            const divPaginations = [...document.querySelectorAll(`.${this.selector.className}-pagination div`)]
            if (divPaginations) {
                divPaginations.forEach((item, index) =>
                    index === this.currentSlide ? item.classList.add('active') : item.classList.remove('active')
                )
            }
        },
    })

    homepage.addPagination()

    setInterval(() => homepage.next(), 5000)

    // document.querySelector('.slider .siema-prev').addEventListener('click', () => .prev())
    // document.querySelector('.slider .siema-next').addEventListener('click', () => .next())
}

if (document.querySelector('.gallery__container')) {
    const homepageGallery = new Siema({
        selector: '.gallery__container',
        duration: 500,
        easing: 'ease-out',
        perPage: {
            '768': 2,
            '992': 3,
            '1200': 4
        },
        startIndex: 0,
        threshold: 20,
        loop: true,
        onInit: () => {},
        onChange: () => {},
    })
    setInterval(() => homepageGallery.next(), 3000)
}
