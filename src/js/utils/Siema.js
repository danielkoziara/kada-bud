import Siema from 'siema'

Siema.prototype.addPagination = function () {
    const container = document.createElement('div')
    container.classList.add(this.selector.className + '-pagination')
    for (let i = 0; i < this.innerElements.length; i++) {
        this.selector.appendChild(container)
        const btn = document.createElement('div')
        if (i === 0) {
            btn.classList.add('active')
        }
        // btn.textContent = i
        btn.addEventListener('click', () => this.goTo(i))
        container.appendChild(btn)
    }
}

export default Siema
